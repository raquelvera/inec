package com.inec.server.model.process;

import java.util.List;

import javax.jdo.PersistenceManager;

import com.google.appengine.api.datastore.KeyFactory;
import com.inec.server.model.bean.UsuarioFiscalizador;
import com.inec.server.model.bean.Zona;
import com.inec.server.model.dao.PMF;
import com.inec.server.model.logic.LogicUsuarioFiscalizador;
import com.inec.server.model.logic.LogicZona;
import com.inec.shared.BeanParametro;
import com.inec.shared.Message;
import com.inec.shared.UnknownException;

public class GestionUsuario {

	private final static String INSERTAR = "I";
	private final static String ACTUALIZAR = "A";

	/**
	 * 
	 * @param dni
	 * @param nombres
	 * @param apellidos
	 * @param correoPersonal
	 * @param correoInstitucional
	 * @param telefono
	 * @param codeZona
	 * @return
	 * @throws UnknownException
	 */
	public static Boolean crearUsuario(String dni, String nombres, String apellidos, String correoPersonal,
			String correoInstitucional, String telefono, String codeZona) throws UnknownException {
		PersistenceManager pm = null;
		try {
			pm = PMF.getPMF().getPersistenceManager();

			LogicUsuarioFiscalizador logicUsuarioFiscalizador = new LogicUsuarioFiscalizador(pm);
			LogicZona logicZona = new LogicZona(pm);

			UsuarioFiscalizador beanUsuarioFiscalizador = (UsuarioFiscalizador) logicUsuarioFiscalizador
					.getBeanByDni(dni);
			if (beanUsuarioFiscalizador != null) {
				GestionShared.closeConnection(pm, null);
				throw new UnknownException(Message.ERROR_EXISTENCIA);
			}
			Zona beanZona = logicZona.getBeanByCode(codeZona);
			if (beanZona == null) {
				GestionShared.closeConnection(pm, null);
				throw new UnknownException(Message.ERROR_NO_EXISTENCIA);
			}
			beanUsuarioFiscalizador = new UsuarioFiscalizador();
			beanUsuarioFiscalizador.setIdUsuarioFiscalizador(
					KeyFactory.keyToString(KeyFactory.createKey(UsuarioFiscalizador.class.getSimpleName(), dni)));
			beanUsuarioFiscalizador.setCodeUsuarioFiscalizador(dni);
			beanUsuarioFiscalizador.setDniFiscalizador(dni);
			beanUsuarioFiscalizador.setNombre(nombres);
			beanUsuarioFiscalizador.setApellido(apellidos);
			beanUsuarioFiscalizador.setCorreoPersonal(correoPersonal);
			beanUsuarioFiscalizador.setCorreoCorporativo(correoInstitucional);
			beanUsuarioFiscalizador.setNumero(telefono);
			beanUsuarioFiscalizador.setCodeZona(codeZona);
			beanUsuarioFiscalizador.setVersion(new java.util.Date().getTime());

			BeanParametro beanParametro = new BeanParametro();
			beanParametro.setTipoOperacion(INSERTAR);
			beanParametro.setBean(beanUsuarioFiscalizador);
			Boolean rptaUsuarioFiscalizador = logicUsuarioFiscalizador.mantenimiento(beanParametro);
			if (!rptaUsuarioFiscalizador) {
				throw new UnknownException(Message.ERROR_OPERACION);
			}
			return true;
		} catch (Exception ex) {
			throw new UnknownException(ex.getMessage());
		} finally {
			GestionShared.closeConnection(pm, null);
		}
	}
	
	/**
	 * 
	 * @return
	 * @throws UnknownException
	 */
	public static List<UsuarioFiscalizador> listarUsuarios()throws UnknownException{
		PersistenceManager pm=null;
		try{
			pm = PMF.getPMF().getPersistenceManager();
			LogicUsuarioFiscalizador logicUsuarioFiscalizador = new LogicUsuarioFiscalizador(pm);		
			return (List<UsuarioFiscalizador>) logicUsuarioFiscalizador.getListarBean();
		}catch(Exception ex){
			throw new UnknownException(ex.getMessage());
		}finally{
			GestionShared.closeConnection(pm, null);
		}
	}
}
