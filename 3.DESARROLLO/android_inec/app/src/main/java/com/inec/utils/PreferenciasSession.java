package com.inec.utils;

import android.content.SharedPreferences;

/**
 * Created by root on 09/10/16.
 */

public class PreferenciasSession {

    public static final String NAME_PREFERENCE="prefSesion";
    public static final String URL_PROFILE_DEFECTO="http://assets.designspark.com/v78/bundles/designsparkdesignandmake/img/default-avatar.jpg";
    public static final String FLICKR_USER_ID="143813147@N06";
    public static final String FLICKR_USER_NAME = "Kiongo Technology";
    public static final String FLICKR_API_KEY="ccce1fb35a9f48f2b5f041ab7cc06b3b";
    public static final String FLICKR_API_SECRET="b5c347e00a767502";
    public static final String FLICKR_OAUTH_TOKEN="72157673833979901-2f57df0d9eb9cdf7";
    public static final String FLICKR_OAUTH_SECRET="ac3b5023e5420e4d";
    public static final String URL_IMAGEN_NO_ENCONTRADA="http://seo.tehnoseo.ru/img/not-available.png";
    public static final String URL_IMAGEN_PORTADA="https://c1.staticflickr.com/9/8597/28572513360_cd0d4b15f5_z.jpg";
    public static final String API_KEY_GOOGLE_MAP="AIzaSyD5E3eIAZitWUAOhBMUgK9BS58DAIJbCY0";
    public static final String TWITTER_KEY = "f7qrqc5rXINYSPqwgO2pX1s8j";
    public static final String TWITTER_SECRET = "Zd0RjymSHhbsfqqlhwv9uC1NA9OaQRyfmxQFmsiT8b1EhRZ2WE";
    public static final String FACEBOOK_KEY = "815015101964527";
    public static final String FACEBOOK_SECRET = "2c8d793649342d44e91ae2be6e545e80";
    public static final String GOOGLE_PLUS_KEY = "728034091568-evg6hf76777q7dmnbulu77la9k49jc8j.apps.googleusercontent.com";
    public static final String GOOGLE_PLUS_SECRET = "38D05E8A0L2_6EH-zFnfeYsl";
    public static final String ENDPOINT="webindiant.appspot.com";
    public static final String PAISNACIMIENTODEFAULT="PERU";
    public static final String REGIONNACIMIENTODEFAULT="LAMBAYEQUE";
    public static final String LOCALIDADNACIMIENTODEFAULT="CHICLAYO";
    public static final String REGISTROTIPOOAUTH="OAUTH";
    public static final String REDSOCIALFACEBOOK="FACEBOOK:INDIANT";
    public static final String REDSOCIALTWITTER="TWITTER:INDIANT";
    public static final String REDSOCIALGOOGLE="GOOGLE:INDIANT";
    public static final String REDSOCIALFLICKR="FLICKR:INDIANT";
    public static final String GENREDTOKENFIRE="GENREDTOKENFIRE";
    public static final String TOKENFIREBASE="TOKENFIREBASE";
    public static final String DEFAULT_ROOT_URL = "https://webindianna.appspot.com/_ah/api/";


    public static void almacenarValor(SharedPreferences prefSesion, String name, String value){
        SharedPreferences.Editor editor =prefSesion.edit();
        editor.putString(name,value);
        editor.commit();
    }

    public static void almacenarValorBoolean(SharedPreferences prefSesion, String name, boolean value){
        SharedPreferences.Editor editor =prefSesion.edit();
        editor.putBoolean(name,value);
        editor.commit();
    }

    public static void remove(SharedPreferences prefSesion) {
        SharedPreferences.Editor editor = prefSesion.edit();
        editor.clear();
        editor.commit();
    }
}
