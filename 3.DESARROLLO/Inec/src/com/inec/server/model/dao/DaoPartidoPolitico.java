package com.inec.server.model.dao;

import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;

import com.inec.server.model.bean.PartidoPolitico;
import com.inec.shared.BeanParametro;
import com.inec.shared.UnknownException;

public class DaoPartidoPolitico {
	private PersistenceManager pm;

	public DaoPartidoPolitico(PersistenceManager pm) {
		this.pm = pm;
	}

	public boolean mantenimiento(BeanParametro parametro)
			throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.mantenimiento(parametro);
	}
	public boolean mantenimiento(List<BeanParametro> listParametros)
			throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.mantenimiento(listParametros);
	}
	
	public Object getBean(String id) throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.getBean(PartidoPolitico.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<PartidoPolitico> getListarBean() throws UnknownException {
		Querys query = new Querys(this.pm);
		Collection<PartidoPolitico> lista = (Collection<PartidoPolitico>) query
				.getListaBean(PartidoPolitico.class);
		return lista;
	}	
}
