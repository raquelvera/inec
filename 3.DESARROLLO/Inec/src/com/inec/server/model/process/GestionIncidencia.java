package com.inec.server.model.process;

import javax.jdo.PersistenceManager;

import com.inec.server.model.dao.PMF;
import com.inec.server.model.logic.LogicFotografia;
import com.inec.server.model.logic.LogicIncidencia;
import com.inec.shared.UnknownException;

public class GestionIncidencia {

	public static Boolean registrarIncidencia(
			String foto,
			String departamento,
			String provincia,
			String distrito,
			String direccion,
			String partido,
			String tipoIncidencia,
			String descripcion)throws UnknownException{
		PersistenceManager pm=null;
		try{
			pm = PMF.getPMF().getPersistenceManager();
			LogicFotografia logicFotografia= new LogicFotografia(pm);
			LogicIncidencia logicIncidencia= new LogicIncidencia(pm);
			
			
			return true;
		}catch(Exception ex){
			throw new UnknownException(ex.getMessage());
		}finally{
			GestionShared.closeConnection(pm, null);
		}
	}
	
	public static Boolean listarIncidencias(
			String foto,
			String departamento,
			String provincia,
			String distrito,
			String direccion,
			String partido,
			String tipoIncidencia,
			String descripcion)throws UnknownException{
		PersistenceManager pm=null;
		try{
			pm = PMF.getPMF().getPersistenceManager();
			
			return true;
		}catch(Exception ex){
			throw new UnknownException(ex.getMessage());
		}finally{
			GestionShared.closeConnection(pm, null);
		}
	}
}
